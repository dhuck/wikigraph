class Edge:

    def __init__(self, dest: str = None, cost: int = None):
        self.__source = dest if dest is not None else ""
        self.__cost = cost if cost is not None else 0

    def increase_cost(self):
        self.__cost += 1
