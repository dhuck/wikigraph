class Vertex:

    def __init__(self, name: str = None, adjacent: list = None, id: int = None):
        self.__adjacent: list = adjacent if adjacent is not None else list()
        self.__name: str = name if name is not None else ""
        self.__id: int = id if id is not None else -1
        self.scratch = 0

    def get_name(self) -> str:
        return self.__name

    def get_adjacent(self) -> list:
        return self.__adjacent
