import random
import threading

import pymongo as mongo
from pymongo import MongoClient
from rich.console import Console

from config import get_config
from crawler import WikiCrawler

SET_LOCK = threading.Lock()
START_PAGE: list = ["Carnegie Mellon University", "Massachusetts Institute of Technology", "Stanford University",
                    "University of California, Berkeley", "University of Illinois at Urbana–Champaign",
                    "Cornell University", "University of Washington", "Georgia Tech", "Princeton University",
                    "University of Texas at Austin", "Harry Potter", "Doctor Who", "Star Wars", "The Lord of the Rings",
                    "Star Trek", "Marvel", "Pokémon", "My Little Pony", "Among Us", "Minecraft"]

REST: float = 0.05
THREAD_LIMIT = 12
TOTAL_ARTICLES = 6_209_923

# start a universal rich console.
con = Console()


def get_collection() -> mongo.collection.Collection:
    config = get_config()
    mongo_cred = f"{config['mongo_user']}:{config['mongo_pass']}"
    mongo_url = f"{config['mongo_host']}:{config['mongo_port']}"
    mc = MongoClient(f"mongodb://{mongo_cred}@{mongo_url}")
    db = mc[config["db"]]
    collection = db[config["collection"]]
    return collection


def main():
    col = get_collection()
    size = col.estimated_document_count()
    docs = col.find()
    start_pages = START_PAGE
    con.log("Getting Start Pages")
    crawler = None
    try:
        # uncomment for pulling into a partial database
        # for i in range(THREAD_LIMIT // 4):
        #     page = docs[random.randint(0, size)]["title"]
        #     start_pages.append(page)
        con.log("Starting Crawler")
        crawler = WikiCrawler(col, start_pages, workers=THREAD_LIMIT, rest=REST, con=con)
        crawler.start()
    except KeyboardInterrupt:
        if crawler is not None:
            crawler.quit()
        con.log("Goodbye")


if __name__ == '__main__':
    main()
