import re
import time
from concurrent.futures import ThreadPoolExecutor
from queue import Queue, Full

from pymongo.collection import Collection
from pymongo.errors import DuplicateKeyError
from rich.progress import Progress, BarColumn, TransferSpeedColumn, TimeRemainingColumn
from wikipediaapi import Wikipedia

import datetime as dt

WIKIPEDIA_SIZE = 6_209_923


class WikiCrawler:
    def __init__(self, collection: Collection, start_pages: list, workers=12, rest=0.4, con=None):
        self.__workers = workers
        self.__queue_list = [Queue() for _ in range(workers)]
        self.__main_queue = Queue()
        self.__db_queue = Queue()
        self.__worker_pool = ThreadPoolExecutor(max_workers=workers * 2)
        self.__crawler_pool = ThreadPoolExecutor(max_workers=workers)

        self.__continue = True
        self.__progress = self.__progress = Progress("[progress.description]{task.description}",
                                                     BarColumn(style="color(0)", bar_width=None,
                                                               complete_style="color(1)"),
                                                     "[progress.percentage]{task.percentage:>02.2f}%",
                                                     TransferSpeedColumn(),
                                                     TimeRemainingColumn(),
                                                     console=con)

        self.__wikipedia = Wikipedia('en')
        self.__titles = set()
        self.__enqueued = set()
        self.__prunes = [set() for _ in range(self.__workers)]
        self.__rest = rest
        self.__queue_timeout = 2
        self.__batch_size = 32
        self.__update_timer = 5
        self.__db = collection
        self.__console = con

        self.__prune_freq = 120

        self.__setup(start_pages)

    def __setup(self, start_pages):
        with self.__progress:

            self.__console.log("Setting up the titles set")
            agg = self.__db.aggregate([{"$group": {"_id": "$title"}}])
            pid = self.__progress.add_task("Title Set", total=self.__db.estimated_document_count())
            for page in agg:
                self.__titles.add(page["_id"])
                self.__progress.update(pid, advance=1)
            self.__progress.remove_task(pid)

            self.__console.log("Building initial Queue from start_pages")
            start_queue = Queue()
            for page in start_pages:
                self.__enqueued.add(page)
                start_queue.put(page)

            pid = self.__progress.add_task("Initial Queue", total=start_queue.qsize())
            while not start_queue.empty():
                page = start_queue.get()
                self.__fetch_page(page)
                self.__progress.update(pid, advance=1)
            self.__progress.remove_task(pid)

        self.__console.log("All set up and ready to party!")
        self.__console.log(f"Queue Size: {self.__main_queue.qsize():,}")
        self.__console.log(f"Title Size: {len(self.__titles):,}")

    def __crawler(self, worker_id: int):
        count = 0
        while self.__continue:
            page = self.__queue_list[worker_id].get()
            if page not in self.__titles:
                try:
                    self.__fetch_page(page)
                except Exception as e:
                    self.__console.log(e)
                    self.__console.log(f"Hit an api pushback on {page}")
                    self.__main_queue.put(page)
                    time.sleep(5)
                    self.__rest += 0.002
                time.sleep(self.__rest)
            count += 1

    def __fetch_page(self, page):
        curr_page = self.__wikipedia.page(page)
        links = list()
        if curr_page.exists():
            links = self.__clean_links(curr_page.links)
            pageid = curr_page.pageid
            wiki = {
                "pageid": pageid,
                "title": curr_page.title,
                "summary": curr_page.summary,
                "links": links,
                "full_text": None,
                "last_update": dt.datetime.utcnow()
            }
            self.__db_queue.put(wiki)

            self.__titles.add(pageid)
            self.__titles.add(page)
            self.__enqueued.remove(page)

            if len(links) > 0:
                for link in links:
                    if link not in self.__enqueued:
                        self.__main_queue.put(link, timeout=self.__queue_timeout)
                        self.__enqueued.add(link)

    def __queue_worker(self):
        while self.__continue:
            for q in self.__queue_list:
                q.put(self.__main_queue.get())

    def __database_worker(self):
        while self.__continue:
            page = self.__db_queue.get()
            try:
                self.__db.insert_one(page)
            except DuplicateKeyError:
                self.__titles.add(page["title"])
                # self.__console("wut?")
            except Exception as e:
                self.__console.log(e)

    @staticmethod
    def __clean_links(links):
        pattern = "[A-z\s]+:"  # Filter out the meta links
        out: list = list()

        for name in links.keys():
            if re.match(pattern, name) is None:
                out.append(name)
        return out

    def __update(self, message=None):
        qsize = 0
        for q in self.__queue_list:
            qsize += q.qsize()

        qsize += self.__main_queue.qsize()

        self.__console.log(
            f"[TR: {self.__db.estimated_document_count():,}]"
            f"[TT: {len(self.__titles):,}]"
            f"[QS: {qsize:,}]"
            f"[MQ: {self.__main_queue.qsize():,}]"
            f"[DBQ: {self.__db_queue.qsize():,}]"
        )

    def pruner(self, wid):
        while self.__continue:
            with ThreadPoolExecutor(max_workers=self.__workers // 4) as pruners:
                self.__prunes[wid] = set()
                for i in range(self.__workers // 4):
                    pruners.submit(self.__prune_queue, wid)
                pruners.shutdown(wait=True)
            with ThreadPoolExecutor(max_workers=self.__workers // 4) as pruners:
                for i in range(self.__workers // 4):
                    pruners.submit(self.__rebuild_queue, wid)
                pruners.shutdown(wait=True)
            time.sleep(120)

    def __prune_queue(self, wid):
        while not self.__queue_list[wid].empty():
            page = self.__queue_list[wid].get()
            if page not in self.__titles:
                self.__prunes[wid].add(page)
                self.__enqueued.add(page)

    def __rebuild_queue(self, wid):
        while len(self.__prunes[wid]) > 0:
            page = self.__prunes[wid].pop()
            try:
                self.__queue_list[wid].put(page)
            except Full:
                pass

    def start(self):
        self.__console.log("Starting the crawler")
        with self.__progress:
            for worker_id in range(self.__workers):
                self.__crawler_pool.submit(self.__crawler, worker_id)
                self.__crawler_pool.submit(self.__crawler, worker_id)
            for _ in range(self.__workers // 2):
                self.__worker_pool.submit(self.__queue_worker)
            self.__worker_pool.submit(self.__database_worker)
            pid = self.__progress.add_task("Total Progress", total=WIKIPEDIA_SIZE)
            self.__worker_pool.submit(self.__status_report, pid)

            for worker_id in range(self.__workers):
                self.__worker_pool.submit(self.pruner, worker_id)
                time.sleep(self.__prune_freq // self.__workers)
            while self.__continue:
                time.sleep(1)  # keep the context alive

    def __status_report(self, pid):
        ticker = 0
        while self.__continue:
            if ticker % self.__update_timer == 0:
                self.__update()
                ticker = 0
            articles = self.__db.estimated_document_count()
            self.__progress.update(pid, completed=articles)
            time.sleep(1)
            ticker += 1
        # self.__progress.remove_task(pid)

    def quit(self):
        self.__console.log("Exit command received, shutting down...")
        self.__continue = False
        self.__worker_pool.shutdown(wait=False)
        self.__crawler_pool.shutdown(wait=True)
