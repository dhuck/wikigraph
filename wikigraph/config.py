import configparser


def get_config():
    c = configparser.ConfigParser()
    c.read('wikigraph.conf')
    out = dict()
    for section in c:
        for key in c[section]:
            out[key] = c[section][key]
    return out


if __name__ == '__main__':
    config = get_config()
    print(config)
